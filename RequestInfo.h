#ifndef REQUESTINFO_H
#define REQUESTINFO_H
#include<QtCore>
#include<QtNetwork>

class RequestInfo{

public:
    int type;
    bool isDownload;
    bool multiform_data;
    QJsonArray multiData;
    QString name;
    QUrl url;
    QString queriedParams;
    QMap<QString,QString> params;

    QNetworkRequest get(){
        QNetworkRequest request;
        
        if(url.hasQuery()){
            request.setUrl(url);
        }else{
            request.setUrl(QUrl(QString("%1?%2").arg(url.toDisplayString()).
                                arg(queriedParams)));
       }
        return request;
    }

    QNetworkRequest get(QUrl url,bool isDownload = false){
        QNetworkRequest request;
        request.setUrl(url);
        return request;
    }
};

#endif // REQUESTINFO_H
