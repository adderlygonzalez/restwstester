#include "wstester.h"
#include "time.h"

WsTester::WsTester(){
   tloader = new TestLoader;
   netManager = new QNetworkAccessManager(this);
}

WsTester::~WsTester(){
    delete tloader;
}

void WsTester::prepare(QString input,QString output){
    inputFile = input;
    outputfile = output;

    if(outputfileExists() && !overrideFile){
        diagMessage("Output file exists!");
        exitApp();
        return;
    }

    connect(tloader,SIGNAL(loaded()),this,SLOT(ready()));
    connect(tloader,SIGNAL(errorLoading(QString)),this,SLOT(diagMessage(QString)));
    connect(netManager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onRequestFinished(QNetworkReply*)));
    tloader->load(inputFile);
    requestAmount = tloader->_requests.length();
}

void WsTester::prepare(QJsonObject &obj)
{
    outputfile = obj.contains("output") ? obj.value("output").toString():"TODO.GENERATEFILENAME";
    connect(tloader,SIGNAL(loaded()),this,SLOT(ready()));
    connect(tloader,SIGNAL(errorLoading(QString)),this,SLOT(diagMessage(QString)));
    connect(netManager,SIGNAL(finished(QNetworkReply*)),this,SLOT(onRequestFinished(QNetworkReply*)));
    tloader->load(obj);
    requestAmount = tloader->_requests.length();
}

void WsTester::prepareRequests()
{
    QList<RequestInfo>::Iterator
            it = tloader->_requests.begin();
    QNetworkRequest request;
    QNetworkReply* reply = NULL;
    QHttpMultiPart* multipart = NULL;

    for(;it != tloader->_requests.end();it++)
    {
        switch ((*it).type)
        {
        case QNetworkAccessManager::HeadOperation:{
            request.setUrl(QUrl(QString("%1?%2").arg((*it).url.toString()).
                                arg((*it).queriedParams)));
            reply = netManager->head(request);
        }
            break;
        case QNetworkAccessManager::GetOperation:{

            if((*it).url.hasQuery()){
                request.setUrl((*it).url);
            }else{
                request.setUrl(QUrl(QString("%1?%2").arg((*it).url.toDisplayString()).
                                    arg((*it).queriedParams)));
            }
            reply = netManager->get(request);
        }
            break;
        case QNetworkAccessManager::PutOperation:
        case QNetworkAccessManager::PostOperation:{
            request.setUrl((*it).url);
            if((*it).multiform_data){
                multipart = prepareMultidata((*it));

                if((*it).type == QNetworkAccessManager::PostOperation)
                    reply = netManager->post(request,multipart);
                else if((*it).type == QNetworkAccessManager::PutOperation)
                    reply = netManager->put(request,multipart);
                else {} //TODO: wtf is happening here
                multipart->setParent(reply); //pass ownership to reply
            }else{
                QByteArray params;
                params.append((*it).queriedParams);
                reply = netManager->post(request,params);
                if((*it).type == QNetworkAccessManager::PostOperation)
                    reply = netManager->post(request,params);
                else if((*it).type == QNetworkAccessManager::PutOperation)
                    reply = netManager->put(request,params);
            }
        }
            break;
        case QNetworkAccessManager::DeleteOperation:{
            request.setUrl(QUrl(QString("%1?%2").arg((*it).url.toString()).
                                arg((*it).queriedParams)));
            reply = netManager->deleteResource(request);
        }
            break;
        default:{
            //TODO: Invalid request type
            qDebug() << "Ay Mami! Not type ";
            prepared = false;
            return;
        }
            break;
        }
        if(reply!=NULL){
            reply->setProperty("begin",QString("%1").arg(clock()));//benchmarking
            reply->setProperty("name",(*it).name);
            if((*it).isDownload){
                reply->setProperty("download",QVariant(true));
            }
            connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(DownloadProgress(qint64,qint64)));
            timer = new QTime();
            timer->start();
        }
    }
}
/**
* This prepare a multidata part from the next json structure.
*
*Request{
* "multiform-data":[0 = yes,1=no],
* "multi-data":[
*    {
*       "content-type":"plain/text",
*       "name":"test-name"
*     },
*    {
*       "content-type":"image/jpeg",
*       "name":"image-name",
*       "url":"./ouyeah.jpeg"
*     }
*   ]
* }
*/
QHttpMultiPart* WsTester::prepareMultidata(RequestInfo& rinfo)
{
    QHttpMultiPart* multipart = NULL;

    if(rinfo.multiform_data && !rinfo.multiData.isEmpty()){
        multipart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

        QJsonArray::Iterator multiIt = rinfo.multiData.begin();
        QJsonObject multiData;
        QString partname;
        QString partpath;
        QString partContentType;

        for(;multiIt!= rinfo.multiData.end();multiIt++){
            multiData = (*multiIt).toObject();

            partname = multiData.value("name").toString();
            partpath = multiData.value("url").toString();
            partContentType = multiData.value("content-type").toString();

            if(partContentType == "text/plain"){
                QHttpPart textpart;
                textpart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                   QVariant(QString("form-data; name=\"%1\"").arg(partname)));
                multipart->append(textpart);

            }else if(partContentType == "image/jpeg"){
                QHttpPart imagepart;
                imagepart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
                imagepart.setHeader(QNetworkRequest::ContentDispositionHeader, 
                                    QVariant(QString("form-data; name=\"%1\"").arg(partname)));
                QFile* file = new QFile(partpath);
                file->open(QIODevice::ReadOnly);
                file->setParent(multipart);
            }
        }
    }
    return multipart;
}

void WsTester::saveResponse(QNetworkReply *reply)
{
    reply->setProperty("end",QString("%1").arg(clock())); //bgenchmarking purposes

    QJsonObject response; //will contain all the information about the request
    QJsonObject headerJson;

    //QNetworkRequest request = reply->request();

    QByteArray data = reply->readAll();
    QString errorString = reply->errorString();
    QString url = reply->url().toString();
    QList<QNetworkReply::RawHeaderPair> headers = reply->rawHeaderPairs();

    QList<QNetworkReply::RawHeaderPair>::ConstIterator it;

    for(it = headers.constBegin();it!=headers.constEnd();it++){
        headerJson.insert(QString((*it).first),QString((*it).second));
#ifdef QT_DEBUG
        qDebug() << QString((*it).first) << QString((*it).second);
#endif
    }

    switch (reply->error()) {
        case QNetworkReply::NoError:
            break;
        default:
            break;
    }

    qlonglong beginTime = reply->property("begin").toLongLong();
    qlonglong endTime = reply->property("end").toLongLong();
    response.insert("name",reply->property("name").isValid() ?
                        reply->property("name").toString():
                        QString(""));
    response.insert("URL",url);
    response.insert("data",QString(data));
    response.insert("errorS      tring",errorString);
    response.insert("headers",headerJson);
    response.insert("executionTime",endTime-beginTime);

    if(showProgress)
        printObject(response);

    testsResponses.append(response);

    requestAmount--;
    if(requestAmount == 0){
        saveTest();
    }
}

void WsTester::saveDownloadedFile(QNetworkReply *reply)
{
    printReplyError(reply);
    QByteArray barray;
    barray = reply->readAll();

    QString requestName = reply->property("name").isValid()?
                reply->property("name").toString(): QString("UNTITLED_FILE");
    QFile outputfile(saveFileName(reply->url()));
    if(!outputfile.open(QIODevice::WriteOnly)){
        //ERROR on output
        qDebug() << "Could not save downloaded file";
    }
    outputfile.write(barray.constData(),barray.size());
    outputfile.close();

}
void WsTester::DownloadProgress(qint64 bytesReceived, qint64 bytesTotal) {
    // Convert to integers.
    int br = bytesReceived;
    int bt = bytesTotal;

    // Get speed. Borrowed from QT Documentation.
    double speed = bytesReceived * 1000.0 / timer->elapsed();
    QString unit;
    if (speed < 1024) {
        unit = "bytes/sec";
    } else if (speed < 1024*1024) {
        speed /= 1024;
        unit = "kB/s";
    } else {
        speed /= 1024*1024;
        unit = "MB/s";
    }

    QString speedString = QString::number(speed);
    qDebug() << "Downloading at " << speed << " " << unit;
    //emit Progress(br, bt, "Downloading at " + speedString + " " + unit);
}

QString WsTester::saveFileName(QUrl url) {
    // Borrowed from the QT Examples

    QString path = url.path();
    QString basename = QFileInfo(path).fileName();

    if (basename.isEmpty())
        basename = "download";

    if (QFile::exists(basename)) {
        // already exists, don't overwrite
        int i = 0;
        basename += '.';
        while (QFile::exists(basename + QString::number(i)))
            ++i;

        basename += QString::number(i);
    }

    basename = basename.prepend("./");

    return basename;
}

void WsTester::printReplyError(QNetworkReply *reply)
{
    if(reply->error() != QNetworkReply::NoError){
        qDebug() << reply->errorString();
    }
}

bool WsTester::outputfileExists(){
    QFile file(outputfile);
    return file.exists();
}

void WsTester::saveTest()
{
    QDateTime time;
    QFile file(QString("%1").arg(outputfile));

    if(!file.open(QIODevice::WriteOnly| QIODevice::Truncate)){
        qDebug() << "COuld not save responses to new file";
    }
    QTextStream textStream(&file);
    QJsonDocument tmp_doc;
    tmp_doc.setArray(testsResponses);
    textStream << tmp_doc.toJson();

    exitApp();//exit the app
}

void WsTester::ready(){
#ifdef QT_DEBUG
    qDebug() << "it is ready";
    qDebug() << tloader->_requests.length() << " Requests Loaded";
#endif
    prepareRequests();
}

void WsTester::diagMessage(QString msg){
#ifdef QT_DEBUG
    qDebug() << "Ay Mami! ";
    qDebug() << msg;
#endif
}

void WsTester::onRequestFinished(QNetworkReply *reply){
    qDebug() << " Finished ";
    if(reply->property("download").isValid())
        saveDownloadedFile(reply);
    else
        saveResponse(reply);
}
void WsTester::exitApp(){
    QCoreApplication::quit();
}



