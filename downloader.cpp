#include "downloader.h"

Downloader::Downloader()
{
    m_network = new QNetworkAccessManager(this);

    connect(m_network,SIGNAL(finished(QNetworkReply*)),this,SLOT(replied(QNetworkReply*)));
}


QNetworkReply* Downloader::makeRequest(RequestInfo &rinfo)
{
    QNetworkRequest request;
    QNetworkReply* reply = NULL;
    QHttpMultiPart* multipart = NULL;

    switch (rinfo.type)
    {
    case QNetworkAccessManager::HeadOperation:{
        request.setUrl(QUrl(QString("%1?%2").arg(rinfo.url.toString()).
                            arg(rinfo.queriedParams)));
        reply = m_network->head(request);
    }
        break;
    case QNetworkAccessManager::GetOperation:{

        if(rinfo.url.hasQuery()){
            request.setUrl(rinfo.url);
        }else{
            request.setUrl(QUrl(QString("%1?%2").arg(rinfo.url.toDisplayString()).
                                arg(rinfo.queriedParams)));
        }
        reply = m_network->get(request);
    }
        break;
    case QNetworkAccessManager::PutOperation:
    case QNetworkAccessManager::PostOperation:{
        request.setUrl(rinfo.url);
        if(rinfo.multiform_data){
            multipart = prepareMultidata(rinfo);

            if(rinfo.type == QNetworkAccessManager::PostOperation)
                reply = m_network->post(request,multipart);
            else if(rinfo.type == QNetworkAccessManager::PutOperation)
                reply = m_network->put(request,multipart);
            else {} //TODO: wtf is happening here
            multipart->setParent(reply); //pass ownership to reply
        }else{
            QByteArray params;
            params.append(rinfo.queriedParams);
            reply = m_network->post(request,params);
            if(rinfo.type == QNetworkAccessManager::PostOperation)
                reply = m_network->post(request,params);
            else if(rinfo.type == QNetworkAccessManager::PutOperation)
                reply = m_network->put(request,params);
        }
    }
        break;
    case QNetworkAccessManager::DeleteOperation:{
        request.setUrl(QUrl(QString("%1?%2").arg(rinfo.url.toString()).
                            arg(rinfo.queriedParams)));
        reply = m_network->deleteResource(request);
    }
        break;
    default:{
        //TODO: Invalid request type
        qDebug() << "Ay Mami! Not type ";
        //prepared = false;
        return NULL;
    }
        break;
    }
    if(reply!=NULL){
        reply->setProperty("begin",QString("%1").arg(clock()));//benchmarking
        reply->setProperty("name",rinfo.name);
        if(rinfo.isDownload){
            reply->setProperty("download",QVariant(true));
        }
        connect(reply,SIGNAL(downloadProgress(qint64,qint64)),this,SLOT(DownloadProgress(qint64,qint64)));
    }
    return reply;
}

void Downloader::replied(QNetworkReply *reply)
{

}

QHttpMultiPart* Downloader::prepareMultidata(RequestInfo& rinfo)
{
    QHttpMultiPart* multipart = NULL;

    if(rinfo.multiform_data && !rinfo.multiData.isEmpty()){
        multipart = new QHttpMultiPart(QHttpMultiPart::FormDataType);

        QJsonArray::Iterator multiIt = rinfo.multiData.begin();
        QJsonObject multiData;
        QString partname;
        QString partpath;
        QString partContentType;

        for(;multiIt!= rinfo.multiData.end();multiIt++){
            multiData = (*multiIt).toObject();

            partname = multiData.value("name").toString();
            partpath = multiData.value("url").toString();
            partContentType = multiData.value("content-type").toString();

            if(partContentType == "text/plain"){
                QHttpPart textpart;
                textpart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                   QVariant(QString("form-data; name=\"%1\"").arg(partname)));
                multipart->append(textpart);

            }else if(partContentType == "image/jpeg"){
                QHttpPart imagepart;
                imagepart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("image/jpeg"));
                imagepart.setHeader(QNetworkRequest::ContentDispositionHeader,
                                    QVariant(QString("form-data; name=\"%1\"").arg(partname)));
                QFile* file = new QFile(partpath);
                file->open(QIODevice::ReadOnly);
                file->setParent(multipart);
            }
        }
    }
    return multipart;
}


void Downloader::DownloadProgress(qint64 bytesReceived, qint64 bytesTotal){

}

void Downloader::sslErrors(const QList<QSslError> &sslErrors)
{
#ifndef QT_NO_OPENSSL
    foreach (const QSslError &error, sslErrors)
        fprintf(stderr, "SSL error: %s\n", qPrintable(error.errorString()));
#endif
}
