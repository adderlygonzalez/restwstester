#ifndef DOWNLOADER_H
#define DOWNLOADER_H
#include<QtCore>
#include<QtNetwork>
#include<RequestInfo.h>

/**
*   Class for basic request handling.
*/
class Downloader:public QObject
{
    Q_OBJECT
    Q_PROPERTY(QNetworkAccessManager* network  READ getNetwork MEMBER m_network)

public:
    Downloader();
    QNetworkAccessManager* getNetwork(){return m_network;}
    QNetworkReply* makeRequest(RequestInfo& rinfo);
    QHttpMultiPart* prepareMultidata(RequestInfo& rinfo);
    void sslErrors(const QList<QSslError> &sslErrors);

public slots:
    void replied(QNetworkReply* reply);
    void DownloadProgress(qint64 bytesReceived,qint64 bytesTotal);

 signals:

private:
    QNetworkAccessManager* m_network;
    QList<QNetworkReply*>_activeDownloads;
};

#endif // DOWNLOADER_H
