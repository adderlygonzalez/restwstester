#include <QApplication>
#include <QCommandLineParser>
#include "wstester.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QCommandLineParser parser;
    parser.setApplicationDescription("Rest Web services Test");
    parser.addHelpOption();
    parser.addVersionOption();

    parser.addPositionalArgument("source", QCoreApplication::translate("main", "Source file to copy."));
    parser.addPositionalArgument("destination", QCoreApplication::translate("main", "Destination directory."));

    QCommandLineOption showProgressOption("p", QCoreApplication::translate("main", "Verbose application information."));
    parser.addOption(showProgressOption);
    QCommandLineOption forceOption(QStringList() << "f" << "force", "Overwrite existing files.");
    parser.addOption(forceOption);

    parser.process(app);

    QStringList args = parser.positionalArguments();


    if(args.length() < 2){
        qDebug() << " Provide the input and output file: ";
        qDebug() << " USAGE:  restwstester inputfile.json  outputfile ";
    }

    WsTester tter;

    if(parser.isSet(forceOption)){
        tter.overrideFile = true;
    }
    if(parser.isSet(showProgressOption)){
        tter.showProgress = true;
    }

    tter.prepare(args.at(0),args.at(1));


    //QQmlApplicationEngine engine;
    //engine.load(QUrl(QStringLiteral("qrc:///main.qml")));

    return app.exec();
}
