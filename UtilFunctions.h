

#ifndef UTILFUNCTIONS_H
#define UTILFUNCTIONS_H
#include<QString>
#include<QJsonDocument>
#include<QJsonObject>



template<class T>
static QString jsonToQueryString(T &obj)
{
    QString queryStr;

    typename T::ConstIterator it = obj.constBegin();

    bool needConc = false;
    for(;it != obj.constEnd();it++)
    {
        if(needConc) queryStr+="&";
        else needConc=true;
        queryStr+= it.key();
        queryStr+= "=";
        queryStr+=it.value().toString();
    }
    return queryStr;
}


static void printObject(QJsonObject obj, QString id = "sd")
{
  QJsonDocument tmp_doc;
  tmp_doc.setObject(obj);
  qDebug() << id << tmp_doc.toJson();
}

static QString resolveOutputfile()
{

}

#endif // UTILFUNCTIONS_H
