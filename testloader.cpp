#include "testloader.h"

TestLoader::TestLoader()
{
}

void TestLoader::load(QString filename)
{
    QFile file(filename);

    if(!file.open(QIODevice::ReadOnly|QIODevice::Text)){
        emit errorLoading(QString("Error opening %1").arg(filename)); return;
    }

    QJsonDocument json;
    QByteArray byteArray;
    QJsonParseError parser;

    byteArray = file.readAll();

    json = QJsonDocument::fromJson(byteArray,&parser);

    if(parser.error != QJsonParseError::NoError){
        emit errorLoading(QString("Error parsing %1 file,check format").arg(filename));
        return;
    }
    file.close();

    QJsonObject obj = json.object();

    load(obj);
}

void TestLoader::load(QJsonObject &obj)
{
    if(obj.contains("requests") )
    {
        QJsonArray requests= obj.value("requests").toArray();
        QJsonArray::Iterator it = requests.begin();

        for(;it != requests.end();it++)
        {
            QJsonObject request = (*it).toObject();
            addRequest(request);
        }
    }else{
        emit errorLoading(QString("No request proeperty found! "));
        printObject(obj,"Obj with no requests");
        return;
    }
    emit loaded();
}

void TestLoader::addRequest(QJsonObject &request)
{
    QUrl url ;
    QString name;
    QString queryParams;
    QJsonObject params;
    int type;
    bool isDownload = false;
    bool multiform_data = false;
    QJsonArray multiData;

    if(request.contains("url")){
        url.setUrl(request.value("url").toString());
        qDebug() << "URL" << url.toDisplayString();
    }
    if(request.contains("type")){
        type = request.value("type").toInt(0);
    }

    if(request.contains("download")){
        isDownload = true;
    }
    if(request.contains("name")){
        name = request.value("name").toString();
    }
    if(request.contains("multiform-data")){
        multiform_data = request.value("multiform-data").toInt(0) == 0 ? true : false;
        if(multiform_data && request.contains("multi-data")){
            multiData = request.value("multi-data").toArray();
        }
    }
    if(request.contains("params")){
        params = request.value("params").toObject();
    }

    QJsonDocument d;
    d.setObject(params);
//    qDebug()<< "PARAM" << d.toJson();

    RequestInfo rinfo;
    rinfo.url = url;
    rinfo.type= type;
    rinfo.isDownload = isDownload;
    rinfo.multiform_data = multiform_data;
    rinfo.multiData = multiData;
    rinfo.name = name;

    QJsonObject::ConstIterator it = params.constBegin();
    for(;it != params.constEnd();it++){
        rinfo.params.insert(it.key(),it.value().toString());
//        qDebug() << "Param : " << it.key() << " value : "<<it.value().toString();
    }

    //make queried params
    queryParams = jsonToQueryString<QJsonObject>(params);
    rinfo.queriedParams = queryParams;

    qDebug() << "QueriedParams " << queryParams;
    _requests.append(rinfo);

//    QJsonDocument doc;
//    doc.setObject(request);
//    qDebug() << "Parsed Object : ";
//    qDebug() << doc.toJson();
}




