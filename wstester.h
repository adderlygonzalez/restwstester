#ifndef WSTESTER_H
#define WSTESTER_H

#include<QtCore>
#include<QtNetwork>
#include<QJsonDocument>
#include"testloader.h"
#include"UtilFunctions.h"

/**
*  Program to make http requests based on user input for its advance.
*
*  Tester class for the rest webservices.
*   Usage:
*       restwstest inputFile.json outputFileName
*          //tests the request contained in inputfile and outputs it in outputFile
*/
class WsTester: public QObject
{
    Q_OBJECT
public:
    WsTester();
    ~WsTester();

    /**
    *   Prepare input and output file.
    *   Connect some signals, and load and parse requests with @tloader.
    *
    *  On error: it outputs a messages and quits the program.
    */
    void prepare(QString input,QString output);

    /**
    *   @prepare();
    */
    void prepare(QJsonObject &obj);

    /**
    *   Having parsed requests, it created the objects and trigger requests.
    */
    void prepareRequests();

    /**
    *   Save the response for the specific request and response.
    */
    void saveResponse(QNetworkReply* reply);

    /**
    *
    */
    void saveDownloadedFile(QNetworkReply* reply);

    /**
    *   Generates a name for file using the url.
    */
    QString saveFileName(QUrl url);


    /**
    *
    */
    void printReplyError(QNetworkReply* reply);

    /**
    *   If request is multidata form, set the data to the request.
    */
    QHttpMultiPart* prepareMultidata(RequestInfo& rinfo);

    /**
    *   When every request has responded good or bad this save the data to
    * the @outputfile.
    */
    void saveTest();

    /**
    *   Quits the program.
    */
    void exitApp();

    bool overrideFile;//overwrite output file.
    bool showProgress;//output the progress to the console.

public slots:
    void ready(); //loaded the settings
    void diagMessage(QString);
    void onRequestFinished(QNetworkReply*);//will be called everytime a request responds
    void DownloadProgress(qint64 bytesReceived,qint64 bytesTotal);

 signals:
    void cancelTest();

private:
    TestLoader *tloader;
    QList<QNetworkRequest> _requests;
    QList<QNetworkReply*> _replys;// the response of the requests
    QNetworkAccessManager *netManager;
    QJsonArray testsResponses; //the responses that will be saved
    bool prepared;
    int requestAmount;
    QString outputfile;
    QString inputFile;
    QTime* timer;

    bool outputfileExists();//check for file existance
};

#endif // WSTESTER_H
