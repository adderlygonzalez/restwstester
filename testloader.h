#ifndef TESTLOADER_H
#define TESTLOADER_H

#include<QtCore>
#include<QJsonDocument>
#include"UtilFunctions.h"
#include"RequestInfo.h"

/**
*   Loads the tests included in the configuration file.
*
*   It Uses A Json formated file in this Structure:
*
*  {
*    "requests":
*        [
*            {
*                "name":"Request Test",
*                "type":1, [1 = head,2 = get,3 = put,4 = post,5 = delete ]
*                "url":"http://www.google.com",
*                "download":"is this tag exists is enough"
*                "params":
*                        {
*                           "param":"343",
*                           "size":"343"
*                         }
*                 "multiform-data":[0 = yes,1=no],
*                 "multi-data":[
*                    {
*                       "content-type":"plain/text",
*                       "name":"test-name"
*                     },
*                    {
*                       "content-type":"image/jpeg",
*                       "name":"image-name",
*                       "url":"./ouyeah.jpeg"
*                     }
*                   ]
*
*            }
*        ]
*    }
*
*   Notes:
*   On multiform-data requests the params property is ignored,
*   which means all parameters should be specified in the multi-data property.
*/
class TestLoader:public QObject
{
    Q_OBJECT
public:
    TestLoader();

    /**
    *   Loads requests from specified file.
    */
    void load(QString filename = QString(":/requests.json"));

    /**
    *   @load();
    */
    void load(QJsonObject& obj);

    QList<RequestInfo> _requests;

signals:
    /**
    *   Signal is emmited when the loading of the requests is done.
    */
    void loaded();
    /**
    *   Emit a message of error when loading.
    */
    void errorLoading(QString description);

private:    
    /**
    *   Here it parses every request fromt he specified json file.
    *   and add them to the @_requests;
    */
    void addRequest(QJsonObject& request);
};


#endif // TESTLOADER_H
